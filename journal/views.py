from django.shortcuts import render
from django.http import HttpResponseRedirect

from .forms import UserInfo

name = None


def index(request):
    return HttpResponseRedirect('/home')


def user_info_view(request):
    if request.method == 'POST':
        form = UserInfo(request.POST)

        if form.is_valid():
            global name
            name = form.cleaned_data['name']

            return render(request, 'home.html', {'name': name})
        else:
            return render(request, 'home.html', {'form': form})
    else:
        if name:
            return render(request, 'home.html', {'name': name})
        else:
            form = UserInfo()
            return render(request, 'home.html', {'form': form})


def profile_view(request):
    return render(request, 'profile.html', {'name': name})


def key_view(request):
    return render(request, 'key.html', {'name': name})


def this_week_view(request):
    return render(request, 'this_week.html', {'name': name})


def today_view(request):
    return render(request, 'today.html', {'name': name})
