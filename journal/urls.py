from django.urls import path

from .views import *

urlpatterns = [
    path('', index, name='index'),
    path('home', user_info_view, name='home'),
    path('profile', profile_view, name='profile'),
    path('key', key_view, name='key'),
    path('this_week', this_week_view, name='this_week'),
    path('today', today_view, name='today'),
]
